import { useState, useEffect, useReducer } from "react";
import { getEmployees } from "../fetchers/staff";
import { get } from "../actions/staffActions";
import { UserAddIcon } from "@heroicons/react/outline";
import staffReducer from "../reducers/staffReducer";
import StaffList from "../components/employees/StaffList";
import Modal from "../components/Modal";
import CreateEmployee from "../forms/employees/CreateEmployee";
import StatusMessage from "../components/StatusMessage";
import LoadingSpinner from "../components/LoadingSpinner";

export default function StaffPage() {
  // State
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");

  // Reducer
  const [employees, dispatch] = useReducer(staffReducer, []);

  // Handlers
  const handleCreateButtonClick = () => setShowCreateModal(true);
  const handleRetryButtonClick = () => displayEmployees();
  const handleSearchTermChange = e => setSearchTerm(e.target.value);

  /**
   * Gets and displays all the employees.
   *
   * @returns {void}
   */
  const displayEmployees = () => {
    setIsError(false);
    setIsLoading(true);

    getEmployees()
      .then(employees => {
        dispatch(get(employees));

        setIsLoading(false);
      })
      .catch(() => {
        setIsError(true);
      });
  };

  // Get all the employees on app load.
  useEffect(() => {
    displayEmployees();
  }, []);

  // Filter the employees.
  const filteredEmployees = employees.filter(
    employee =>
      `${employee.firstName} ${employee.lastName}`
        .toLowerCase()
        .indexOf(searchTerm.toLowerCase()) !== -1
  );

  return (
    <div className="w-5/6 m-auto">
      <h1 className="heading-main">Staff</h1>

      {isError ? (
        <p className="italic">
          There has been an error in loading the employees.
          <button onClick={handleRetryButtonClick}>Try again.</button>
        </p>
      ) : (
        <>
          <section className="m-auto">
            <button
              className="btn block w-full mx-auto mb-5 mr-0"
              onClick={handleCreateButtonClick}
            >
              <span className="h-6 flex justify-center">
                Create New
                <UserAddIcon className="pl-2" />
              </span>
            </button>

            <div className="rounded-md shadow-lg mb-6">
              <input
                type="text"
                className="input"
                placeholder="Search..."
                value={searchTerm}
                onChange={handleSearchTermChange}
              />
            </div>

            {showCreateModal && (
              <Modal
                title="New Employee"
                XClick={() => setShowCreateModal(false)}
              >
                <CreateEmployee
                  dispatch={dispatch}
                  setShowCreateModal={setShowCreateModal}
                />
              </Modal>
            )}
          </section>

          {isLoading ? (
            <>
              <LoadingSpinner />
              <StatusMessage statusText="Loading the employees..." />
            </>
          ) : (
            <main>
              <StaffList employees={filteredEmployees} dispatch={dispatch} />
            </main>
          )}
        </>
      )}
    </div>
  );
}
