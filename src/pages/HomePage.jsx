export default function HomePage() {
  return (
    <>
      <h1 className="heading-main">Welcome to Staffy!</h1>

      <p className="text-center italic">
        This app will help you kick-start your business!
      </p>
    </>
  );
}
