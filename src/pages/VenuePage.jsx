import { useState, useEffect, useReducer } from "react";
import { getVenues } from "../fetchers/venue";
import { get } from "../actions/venueActions";
import { GlobeAltIcon } from "@heroicons/react/outline";
import venueReducer from "../reducers/venueReducer";
import VenueList from "../components/venues/VenueList";
import Modal from "../components/Modal";
import CreateVenue from "../forms/venues/CreateVenue";
import StatusMessage from "../components/StatusMessage";
import LoadingSpinner from "../components/LoadingSpinner";

export default function VenuePage() {
  // State
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(false);
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");

  // Reducer
  const [venues, dispatch] = useReducer(venueReducer, []);

  // Handlers
  const handleCreateButtonClick = () => setShowCreateModal(true);
  const handleRetryButtonClick = () => displayVenues();
  const handleSearchTermChange = e => setSearchTerm(e.target.value);

  /**
   * Gets and displays all the venues.
   *
   * @returns {void}
   */
  const displayVenues = () => {
    setIsError(false);
    setIsLoading(true);

    getVenues()
      .then(venues => {
        dispatch(get(venues));

        setIsLoading(false);
      })
      .catch(() => {
        setIsError(true);
      });
  };

  // Get all the venues on app load.
  useEffect(() => {
    displayVenues();
  }, []);

  // Filter the venues.
  const filteredVenues = venues.filter(
    venue => venue.name.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1
  );

  return (
    <div className="w-5/6 m-auto">
      <h1 className="heading-main">Venues</h1>

      {isError ? (
        <p className="italic">
          There has been an error in loading the venues.
          <button onClick={handleRetryButtonClick}>Try again.</button>
        </p>
      ) : (
        <>
          <section className="m-auto">
            <button
              className="btn block w-full mx-auto mb-5 mr-0"
              onClick={handleCreateButtonClick}
            >
              <span className="h-6 flex justify-center">
                Create New
                <GlobeAltIcon className="pl-2" />
              </span>
            </button>

            <div className="rounded-md shadow-lg mb-6">
              <input
                type="text"
                className="input"
                placeholder="Search..."
                value={searchTerm}
                onChange={handleSearchTermChange}
              />
            </div>

            {showCreateModal && (
              <Modal title="New Venue" XClick={() => setShowCreateModal(false)}>
                <CreateVenue
                  dispatch={dispatch}
                  setShowCreateModal={setShowCreateModal}
                />
              </Modal>
            )}
          </section>

          {isLoading ? (
            <>
              <LoadingSpinner />
              <StatusMessage statusText="Loading the venues..." />
            </>
          ) : (
            <main>
              <VenueList venues={filteredVenues} dispatch={dispatch} />
            </main>
          )}
        </>
      )}
    </div>
  );
}
