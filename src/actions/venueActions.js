function get(venues) {
  return {
    type: "VENUE_GET",
    payload: venues
  };
}

function add(venue) {
  return {
    type: "VENUE_ADD",
    payload: venue
  };
}

function remove(id) {
  return {
    type: "VENUE_REMOVE",
    payload: { _id: id }
  };
}

function update(updatedVenue) {
  return {
    type: "VENUE_UPDATE",
    payload: updatedVenue
  };
}

export { get, add, remove, update };
