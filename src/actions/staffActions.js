function get(employees) {
  return {
    type: "EMPLOYEE_GET",
    payload: employees
  };
}

function add(employee) {
  return {
    type: "EMPLOYEE_ADD",
    payload: employee
  };
}

function remove(id) {
  return {
    type: "EMPLOYEE_REMOVE",
    payload: { _id: id }
  };
}

function update(updatedEmployee) {
  return {
    type: "EMPLOYEE_UPDATE",
    payload: updatedEmployee
  };
}

export { get, add, remove, update };
