import { removeEmployee } from "../../fetchers/staff";
import { remove } from "../../actions/staffActions";
import { UserRemoveIcon } from "@heroicons/react/outline";
import Modal from "../../components/Modal";

export default function RemoveEmployee({ employee, dispatch, setModalStatus }) {
  const handleFormSubmit = e => {
    e.preventDefault();

    removeEmployee(employee._id).then(message => {
      dispatch(remove(employee._id));
      console.log(message);
    });

    setModalStatus("none");
  };

  return (
    <form onSubmit={handleFormSubmit}>
      <p className="text-center">
        Are you sure you want to delete
        <br />
        <span className="font-bold">{` ${employee.firstName} ${employee.lastName}`}</span>
        ?
      </p>

      <Modal.Footer>
        <Modal.DangerButton>
          Delete
          <UserRemoveIcon className="h-6 pl-2" />
        </Modal.DangerButton>
      </Modal.Footer>
    </form>
  );
}
