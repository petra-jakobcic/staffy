import { useForm } from "react-hook-form";
import { updateEmployee } from "../../fetchers/staff";
import { update } from "../../actions/staffActions";
import { UserCircleIcon } from "@heroicons/react/outline";
import Modal from "../../components/Modal";
import TextInputWithLabel from "../../components/TextInputWithLabel";

export default function EditEmployee({ employee, dispatch, setModalStatus }) {
  const firstNameKey = `${employee._id}-first-name`;
  const lastNameKey = `${employee._id}-last-name`;
  const roleKey = `${employee._id}-role`;

  // Form validation
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({
    defaultValues: {
      [firstNameKey]: employee.firstName,
      [lastNameKey]: employee.lastName,
      [roleKey]: employee.role
    }
  });

  // Saves the information entered in the modal form.
  const handleFormSubmit = data => {
    updateEmployee(employee._id, {
      firstName: data[firstNameKey],
      lastName: data[lastNameKey],
      role: data[roleKey]
    }).then(updatedEmployee => dispatch(update(updatedEmployee)));

    setModalStatus("none");
  };

  return (
    <form onSubmit={handleSubmit(handleFormSubmit)}>
      <TextInputWithLabel
        register={register}
        validationOptions={{ required: true }}
        labelText="First Name:"
        htmlFor={firstNameKey}
        placeholder={employee.firstName}
        autoFocus={true}
      />
      {errors[firstNameKey] && <span>This field is required!</span>}

      <TextInputWithLabel
        register={register}
        validationOptions={{ required: true }}
        labelText="Last Name:"
        htmlFor={lastNameKey}
        placeholder={employee.lastName}
      />
      {errors[lastNameKey] && <span>This field is required!</span>}

      <TextInputWithLabel
        register={register}
        validationOptions={{ required: true }}
        labelText="Role:"
        htmlFor={roleKey}
        placeholder={employee.role}
      />
      {errors[roleKey] && <span>This field is required!</span>}

      <Modal.Footer>
        <Modal.SubmitButton>
          Update
          <UserCircleIcon className="h-6 pl-2" />
        </Modal.SubmitButton>
      </Modal.Footer>
    </form>
  );
}
