import { useState } from "react";
import { useForm } from "react-hook-form";
import { addEmployee, uploadImage } from "../../fetchers/staff";
import { add } from "../../actions/staffActions";
import { UserAddIcon } from "@heroicons/react/outline";
import Modal from "../../components/Modal";
import TextInputWithLabel from "../../components/TextInputWithLabel";
import StatusMessage from "../../components/StatusMessage";
import LoadingSpinner from "../../components/LoadingSpinner";

export default function CreateEmployee({ dispatch, setShowCreateModal }) {
  // State
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [image, setImage] = useState(null);
  const [imageErrorMessage, setImageErrorMessage] = useState(false);

  // Handlers
  const handleImageSelect = e => setImage(e.target.files[0]);

  // Form validation
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm();

  const handleFormSubmit = data => {
    // If the user selects an employee photo...
    if (image) {
      // If the image type is not '.jpeg',
      // don't submit the form.
      if (image.type !== "image/jpeg") {
        setImageErrorMessage(true);
        return;
      }

      setIsSubmitting(true);

      // Wait for Cloudinary to save the image.
      uploadImage(image)
        .then(imageData => {
          // Get the image data from Cloudinary.
          const publicId = imageData.public_id;

          // Save the employee data to our Express API.
          return addEmployee(
            data["first-name"],
            data["last-name"],
            data["role"],
            // The Cloudinary ID of the image.
            publicId
          );
        })
        .then(newEmployee => {
          dispatch(add(newEmployee));

          setIsSubmitting(false);
          setShowCreateModal(false);
        })
        .catch(err => console.error(err));
      // If there is no selected employee photo...
    } else {
      addEmployee(
        // Since the form validation registers with 'htmlFor',
        // use it to access the form data.
        data["first-name"],
        data["last-name"],
        data["role"],
        // No image.
        ""
      )
        .then(newEmployee => {
          dispatch(add(newEmployee));

          setIsSubmitting(false);
          setShowCreateModal(false);
        })
        .catch(err => console.error(err));
    }
  };

  return (
    <form onSubmit={handleSubmit(handleFormSubmit)} className="mx-auto">
      {isSubmitting && (
        <>
          <LoadingSpinner />
          <StatusMessage statusText="Submitting the form..." />
        </>
      )}

      <fieldset disabled={isSubmitting}>
        {/* First name */}
        <TextInputWithLabel
          register={register}
          validationOptions={{ required: true }}
          labelText="First Name:"
          htmlFor="first-name"
          placeholder="First name"
          autoFocus={true}
        />
        {errors["first-name"] && (
          <span className="message-error">This field is required!</span>
        )}

        {/* Last name */}
        <TextInputWithLabel
          register={register}
          validationOptions={{ required: true }}
          labelText="Last Name:"
          htmlFor="last-name"
          placeholder="Last name"
        />
        {errors["last-name"] && (
          <span className="message-error">This field is required!</span>
        )}

        {/* Role */}
        <TextInputWithLabel
          register={register}
          validationOptions={{ required: true }}
          labelText="Role:"
          htmlFor="role"
          placeholder="Role"
        />
        {errors["role"] && (
          <span className="message-error">This field is required!</span>
        )}

        {/* Image upload */}
        <div>
          <label htmlFor="image-upload" className="label">
            Employee Photo: <span className="italic">(optional)</span>
          </label>

          <div className="rounded-md shadow-lg mb-3">
            <input
              type="file"
              accept="image/jpeg"
              onChange={handleImageSelect}
              id="image-upload"
              className="input"
            />
          </div>

          {imageErrorMessage && <p>The image must be in 'jpg/jpeg' format!</p>}
        </div>

        <Modal.Footer>
          <Modal.SubmitButton>
            Create
            <UserAddIcon className="h-6 pl-2" />
          </Modal.SubmitButton>
        </Modal.Footer>
      </fieldset>
    </form>
  );
}
