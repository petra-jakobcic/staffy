import { useForm } from "react-hook-form";
import { updateVenue } from "../../fetchers/venue";
import { update } from "../../actions/venueActions";
import { UserCircleIcon } from "@heroicons/react/outline";
import Modal from "../../components/Modal";
import TextInputWithLabel from "../../components/TextInputWithLabel";

export default function EditVenue({ venue, dispatch, setModalStatus }) {
  const nameKey = `${venue._id}-name`;
  const addressKey = `${venue._id}-address`;
  const postcodeKey = `${venue._id}-postcode`;

  // Form validation
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({
    defaultValues: {
      [nameKey]: venue.name,
      [addressKey]: venue.address,
      [postcodeKey]: venue.postcode
    }
  });

  // Saves the information entered in the modal form.
  const handleFormSubmit = data => {
    updateVenue(venue._id, {
      name: data[nameKey],
      address: data[addressKey],
      postcode: data[postcodeKey]
    }).then(updatedVenue => dispatch(update(updatedVenue)));

    setModalStatus("none");
  };

  return (
    <form onSubmit={handleSubmit(handleFormSubmit)}>
      {/* Name */}
      <TextInputWithLabel
        register={register}
        validationOptions={{ required: true }}
        labelText="Name:"
        htmlFor={nameKey}
        placeholder={venue.name}
        autoFocus={true}
      />
      {errors[nameKey] && <span>This field is required!</span>}

      {/* Address */}
      <TextInputWithLabel
        register={register}
        validationOptions={{ required: true }}
        labelText="Address:"
        htmlFor={addressKey}
        placeholder={venue.address}
      />
      {errors[addressKey] && <span>This field is required!</span>}

      {/* Postcode */}
      <TextInputWithLabel
        register={register}
        validationOptions={{ required: true }}
        labelText="Postcode:"
        htmlFor={postcodeKey}
        placeholder={venue.postcode}
      />
      {errors[postcodeKey] && <span>This field is required!</span>}

      <Modal.Footer>
        <Modal.SubmitButton>
          Update
          <UserCircleIcon className="h-6 pl-2" />
        </Modal.SubmitButton>
      </Modal.Footer>
    </form>
  );
}
