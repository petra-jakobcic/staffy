import { removeVenue } from "../../fetchers/venue";
import { remove } from "../../actions/venueActions";
import { UserRemoveIcon } from "@heroicons/react/outline";
import Modal from "../../components/Modal";

export default function RemoveVenue({ venue, dispatch, setModalStatus }) {
  const handleFormSubmit = e => {
    e.preventDefault();

    removeVenue(venue._id).then(message => {
      dispatch(remove(venue._id));
      console.log(message);
    });

    setModalStatus("none");
  };

  return (
    <form onSubmit={handleFormSubmit}>
      <p className="text-center">
        Are you sure you want to delete
        <br />
        <span className="font-bold">{` ${venue.name}`}</span>?
      </p>

      <Modal.Footer>
        <Modal.DangerButton>
          Delete
          <UserRemoveIcon className="h-6 pl-2" />
        </Modal.DangerButton>
      </Modal.Footer>
    </form>
  );
}
