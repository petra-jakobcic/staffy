import { useState } from "react";
import { useForm } from "react-hook-form";
import { addVenue } from "../../fetchers/venue";
import { add } from "../../actions/venueActions";
import { GlobeAltIcon } from "@heroicons/react/outline";
import Modal from "../../components/Modal";
import TextInputWithLabel from "../../components/TextInputWithLabel";
import StatusMessage from "../../components/StatusMessage";
import LoadingSpinner from "../../components/LoadingSpinner";

export default function CreateVenue({ dispatch, setShowCreateModal }) {
  // State
  const [isSubmitting, setIsSubmitting] = useState(false);

  // Form validation
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm();

  const handleFormSubmit = data => {
    setIsSubmitting(true);

    addVenue(
      // Since the form validation registers with 'htmlFor',
      // use it to access the form data.
      data["name"],
      data["address"],
      data["postcode"]
    )
      .then(newVenue => {
        dispatch(add(newVenue));

        setIsSubmitting(false);
        setShowCreateModal(false);
      })
      .catch(err => console.error(err));
  };

  return (
    <form onSubmit={handleSubmit(handleFormSubmit)} className="mx-auto">
      {isSubmitting && (
        <>
          <LoadingSpinner />
          <StatusMessage statusText="Submitting the form..." />
        </>
      )}

      <fieldset disabled={isSubmitting}>
        {/* Name */}
        <TextInputWithLabel
          register={register}
          validationOptions={{ required: true }}
          labelText="Name:"
          htmlFor="name"
          placeholder="Name"
          autoFocus={true}
        />
        {errors["name"] && (
          <span className="message-error">This field is required!</span>
        )}

        {/* Address */}
        <TextInputWithLabel
          register={register}
          validationOptions={{ required: true }}
          labelText="Address:"
          htmlFor="address"
          placeholder="Address"
        />
        {errors["address"] && (
          <span className="message-error">This field is required!</span>
        )}

        {/* Postcode */}
        <TextInputWithLabel
          register={register}
          validationOptions={{ required: true }}
          labelText="Postcode:"
          htmlFor="postcode"
          placeholder="Postcode"
        />
        {errors["postcode"] && (
          <span className="message-error">This field is required!</span>
        )}

        <Modal.Footer>
          <Modal.SubmitButton>
            Create
            <GlobeAltIcon className="h-6 pl-2" />
          </Modal.SubmitButton>
        </Modal.Footer>
      </fieldset>
    </form>
  );
}
