export default function staffReducer(staffList, action) {
  switch (action.type) {
    // Gets the list of all the employees.
    case "EMPLOYEE_GET":
      return action.payload;

    // Creates a new employee.
    case "EMPLOYEE_ADD":
      return [...staffList, { ...action.payload }];

    case "EMPLOYEE_REMOVE":
      // Removes an employee.
      return staffList.filter(employee => employee._id !== action.payload._id);

    case "EMPLOYEE_UPDATE":
      // Updates the info on an employee.
      return staffList.map(employee =>
        employee._id === action.payload._id ? action.payload : employee
      );

    default:
      return staffList;
  }
}
