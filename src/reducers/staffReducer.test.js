import staffReducer from "./staffReducer";

describe("The 'staffReducer' function", () => {
  test("gets all the employees", () => {
    // Arrange
    const oldState = [];
    const action = {
      type: "EMPLOYEE_GET",
      payload: [{ firstName: "Joe", lastName: "Bloggs" }]
    };

    // Act
    const newState = staffReducer(oldState, action);

    // Assert
    expect(newState).toEqual([{ firstName: "Joe", lastName: "Bloggs" }]);
  });

  test("creates a new employee", () => {
    // Arrange
    const oldState = [
      { firstName: "Joe", lastName: "Bloggs", role: "Line chef" }
    ];
    const action = {
      type: "EMPLOYEE_ADD",
      payload: { firstName: "Luke", lastName: "Skywalker", role: "Master chef" }
    };

    // Act
    const newState = staffReducer(oldState, action);

    // Assert
    expect(newState).toEqual([
      { firstName: "Joe", lastName: "Bloggs", role: "Line chef" },
      { firstName: "Luke", lastName: "Skywalker", role: "Master chef" }
    ]);
  });

  test("removes an employee", () => {
    // Arrange
    const oldState = [
      { firstName: "Joe", lastName: "Bloggs", role: "Line chef", _id: "1" },
      {
        firstName: "Luke",
        lastName: "Skywalker",
        role: "Master chef",
        _id: "2"
      }
    ];
    const action = {
      type: "EMPLOYEE_REMOVE",
      payload: { _id: "2" }
    };

    // Act
    const newState = staffReducer(oldState, action);

    // Assert
    expect(newState).toEqual([
      { firstName: "Joe", lastName: "Bloggs", role: "Line chef", _id: "1" }
    ]);
  });

  test("updates the info on an employee", () => {
    // Arrange
    const oldState = [
      {
        _id: "1",
        firstName: "Joe",
        lastName: "Bloggs",
        role: "Line chef",
        isEditing: true
      },
      {
        _id: "2",
        firstName: "Luke",
        lastName: "Skywalker",
        role: "Master chef"
      }
    ];
    const action = {
      type: "EMPLOYEE_UPDATE",
      payload: {
        _id: "1",
        firstName: "Jessie",
        lastName: "Bliggs",
        role: "Line chef"
      }
    };

    // Act
    const newState = staffReducer(oldState, action);

    // Assert
    expect(newState).toEqual([
      {
        _id: "1",
        firstName: "Jessie",
        lastName: "Bliggs",
        role: "Line chef"
      },
      {
        _id: "2",
        firstName: "Luke",
        lastName: "Skywalker",
        role: "Master chef"
      }
    ]);
  });
});
