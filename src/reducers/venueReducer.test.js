import venueReducer from "./venueReducer";

describe("The 'venueReducer' function", () => {
  test("gets all the venues", () => {
    // Arrange
    const oldState = [];
    const action = {
      type: "VENUE_GET",
      payload: [
        {
          name: "Sticky Fingers",
          address: "Albany Rd 1",
          postcode: "CF24",
          isOneOff: false
        }
      ]
    };

    // Act
    const newState = venueReducer(oldState, action);

    // Assert
    expect(newState).toEqual([
      {
        name: "Sticky Fingers",
        address: "Albany Rd 1",
        postcode: "CF24",
        isOneOff: false
      }
    ]);
  });

  test("creates a new venue", () => {
    // Arrange
    const oldState = [
      {
        name: "Sticky Fingers",
        address: "Albany Rd 1",
        postcode: "CF24",
        isOneOff: false
      }
    ];
    const action = {
      type: "VENUE_ADD",
      payload: {
        name: "Corp Yard",
        address: "Corp Rd 1",
        postcode: "CF10",
        isOneOff: true
      }
    };

    // Act
    const newState = venueReducer(oldState, action);

    // Assert
    expect(newState).toEqual([
      {
        name: "Sticky Fingers",
        address: "Albany Rd 1",
        postcode: "CF24",
        isOneOff: false
      },
      {
        name: "Corp Yard",
        address: "Corp Rd 1",
        postcode: "CF10",
        isOneOff: true
      }
    ]);
  });

  test("removes a venue", () => {
    // Arrange
    const oldState = [
      {
        name: "Sticky Fingers",
        address: "Albany Rd 1",
        postcode: "CF24",
        isOneOff: false,
        _id: "1"
      },
      {
        name: "Corp Yard",
        address: "Corp Rd 1",
        postcode: "CF10",
        isOneOff: true,
        _id: "2"
      }
    ];
    const action = {
      type: "VENUE_REMOVE",
      payload: { _id: "2" }
    };

    // Act
    const newState = venueReducer(oldState, action);

    // Assert
    expect(newState).toEqual([
      {
        name: "Sticky Fingers",
        address: "Albany Rd 1",
        postcode: "CF24",
        isOneOff: false,
        _id: "1"
      }
    ]);
  });

  test("updates the info on a venue", () => {
    // Arrange
    const oldState = [
      {
        name: "Sticky Fingers",
        address: "Albany Rd 1",
        postcode: "CF24",
        isOneOff: false,
        _id: "1"
      },
      {
        name: "Corp Yard",
        address: "Corp Rd 1",
        postcode: "CF10",
        isOneOff: true,
        _id: "2"
      }
    ];
    const action = {
      type: "VENUE_UPDATE",
      payload: {
        name: "DEYA Brewery",
        address: "Albany Rd 1",
        postcode: "CF24",
        isOneOff: false,
        _id: "1"
      }
    };

    // Act
    const newState = venueReducer(oldState, action);

    // Assert
    expect(newState).toEqual([
      {
        name: "DEYA Brewery",
        address: "Albany Rd 1",
        postcode: "CF24",
        isOneOff: false,
        _id: "1"
      },
      {
        name: "Corp Yard",
        address: "Corp Rd 1",
        postcode: "CF10",
        isOneOff: true,
        _id: "2"
      }
    ]);
  });
});
