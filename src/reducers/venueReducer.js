export default function venueReducer(venueList, action) {
  switch (action.type) {
    // Gets the list of all the venues.
    case "VENUE_GET":
      return action.payload;

    // Creates a new venue.
    case "VENUE_ADD":
      return [...venueList, { ...action.payload }];

    case "VENUE_REMOVE":
      // Removes a venue.
      return venueList.filter(venue => venue._id !== action.payload._id);

    case "VENUE_UPDATE":
      // Updates the info on a venue.
      return venueList.map(venue =>
        venue._id === action.payload._id ? action.payload : venue
      );

    default:
      return venueList;
  }
}
