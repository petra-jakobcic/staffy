export default function StatusMessage({ statusText }) {
  return (
    <div className="bg-yellow-100 p-4 my-4 text-center">
      <p>{statusText}</p>
    </div>
  );
}
