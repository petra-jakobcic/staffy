export default function TextInputWithLabel({
  labelText,
  htmlFor,
  placeholder,
  register,
  validationOptions,
  autoFocus = false
}) {
  return (
    <div className="mb-2">
      <label htmlFor={htmlFor} className="label">
        {labelText}
      </label>

      <div className="rounded-md shadow-lg">
        <input
          {...register(htmlFor, validationOptions)}
          type="text"
          id={htmlFor}
          placeholder={placeholder}
          className="input"
          autoFocus={autoFocus}
        />
      </div>
    </div>
  );
}
