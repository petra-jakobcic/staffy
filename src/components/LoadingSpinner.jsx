import { ThreeDots } from "react-loader-spinner";

export default function LoadingSpinner() {
  return (
    <div className="w-full flex justify-center">
      <ThreeDots color="#000" height={30} width={30} />
    </div>
  );
}
