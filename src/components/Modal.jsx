export default function Modal({ children, title, XClick }) {
  // Executes a function when the 'X' in the corner is clicked.
  const handleXClick = () => XClick();

  return (
    <>
      <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
        <div className="relative w-5/6 my-6 mx-auto max-w-3xl">
          {/* content */}
          <div className="p-6 border-0 rounded-lg shadow-lg relative flex flex-col bg-white outline-none focus:outline-none m-auto">
            {/* header */}
            <div className="rounded-t flex flex-col">
              <button className="btn-x" onClick={handleXClick}>
                <span className="bg-transparent text-black h-6 w-6 text-2xl block outline-none focus:outline-none">
                  ×
                </span>
              </button>

              {/* title */}
              <h3 className="mb-10 text-2xl font-semibold text-center">
                {title}
              </h3>
            </div>

            {/* body */}
            <div className="relative flex-auto">{children}</div>
          </div>
        </div>
      </div>

      <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
    </>
  );
}

// Sub-components
Modal.SubmitButton = function ({ children }) {
  return (
    <button className="btn-ok flex justify-center" type="submit">
      {children}
    </button>
  );
};

Modal.DangerButton = function ({ children }) {
  return (
    <button className="btn-danger flex justify-center" type="submit">
      {children}
    </button>
  );
};

Modal.Footer = function ({ children }) {
  return (
    <div className="flex items-center justify-end rounded-b pt-0 mt-8">
      {children}
    </div>
  );
};
