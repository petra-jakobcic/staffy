import Employee from "./Employee";

export default function StaffList({ employees, dispatch }) {
  if (employees.length === 0) {
    return <p>There are no employees.</p>;
  }

  return employees.map(employee => (
    <Employee key={employee._id} employee={employee} dispatch={dispatch} />
  ));
}
