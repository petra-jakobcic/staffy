import { useEffect, useState } from "react";
import { Image, Transformation } from "cloudinary-react";
import { UserCircleIcon } from "@heroicons/react/solid";
import Modal from "../Modal";
import EditEmployee from "../../forms/employees/EditEmployee";
import RemoveEmployee from "../../forms/employees/RemoveEmployee";

export default function Employee({ employee, dispatch }) {
  // State
  const [showDropDownMenu, setShowDropDownMenu] = useState(false);
  const [modalStatus, setModalStatus] = useState("none");

  const handleDropdownButtonClick = () => {
    showDropDownMenu ? setShowDropDownMenu(false) : setShowDropDownMenu(true);
  };

  // When the 'modalStatus' changes,
  // the dropdown menu closes.
  useEffect(() => {
    setShowDropDownMenu(false);
  }, [modalStatus]);

  // Define the modal title depending on the modal status.
  const modalTitle =
    modalStatus === "editing"
      ? `Update ${employee.firstName}`
      : `Delete ${employee.firstName}`;

  return (
    <div className="mb-5 mx-auto rounded-lg border border-gray-200 shadow-md">
      <div className="flex justify-end px-4 pt-4">
        <div className="flex flex-col absolute">
          {/* Dropdown menu button */}
          <button
            onClick={handleDropdownButtonClick}
            id="dropdownButton"
            data-dropdown-toggle="dropdown"
            className="sm:inline-block self-end text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-4 focus:ring-gray-200 rounded-lg text-sm p-1.5"
            type="button"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z" />
            </svg>
          </button>

          {/* Dropdown menu */}
          {showDropDownMenu && (
            <div
              id="dropdown"
              className="relative top-0 right-0 mt-1 bg-white z-10 w-44 text-base list-none rounded divide-y divide-gray-100 shadow"
            >
              <ul className="py-1" aria-labelledby="dropdownButton">
                <li>
                  <button
                    onClick={() => setModalStatus("editing")}
                    className="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 w-full text-left"
                  >
                    Edit
                  </button>
                </li>

                <li>
                  <button
                    onClick={() => setModalStatus("removing")}
                    className="block py-2 px-4 text-sm text-red-600 hover:bg-gray-100 w-full text-left"
                  >
                    Delete
                  </button>
                </li>
              </ul>
            </div>
          )}
        </div>
      </div>

      {/* The modal */}
      {modalStatus !== "none" && (
        <Modal title={modalTitle} XClick={() => setModalStatus("none")}>
          {modalStatus === "editing" && (
            <EditEmployee
              employee={employee}
              dispatch={dispatch}
              setModalStatus={setModalStatus}
            />
          )}

          {modalStatus === "removing" && (
            <RemoveEmployee
              employee={employee}
              dispatch={dispatch}
              setModalStatus={setModalStatus}
            />
          )}
        </Modal>
      )}

      {/* Employee */}
      <div className="flex flex-col items-center pb-10 mt-12">
        {/* The image */}
        {employee.publicId ? (
          <Image
            cloudName="pony-bear"
            publicId={employee.publicId}
            className="mb-3 w-24 h-24 rounded-full shadow-lg"
          >
            <Transformation width="96" height="96" crop="scale" />
          </Image>
        ) : (
          <UserCircleIcon className="mb-3 w-24 h-24 rounded-full shadow-lg opacity-70 text-gray-800" />
        )}
        <h3 className="mb-1 text-xl font-medium text-gray-900">
          {employee.firstName} {employee.lastName}
        </h3>

        <span className="text-sm text-gray-500">{employee.role}</span>

        {/* <div className="flex mt-4 space-x-3 lg:mt-6">
          <span className="inline-flex items-center py-2 px-4 text-sm font-medium text-center text-gray-900 bg-white rounded-lg border border-gray-300 hover:bg-gray-100 focus:ring-4 focus:ring-blue-300">
            Message
          </span>
        </div> */}
      </div>
    </div>
  );
}
