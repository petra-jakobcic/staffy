import { useEffect, useState } from "react";
import Modal from "../Modal";
import EditVenue from "../../forms/venues/EditVenue";
import RemoveVenue from "../../forms/venues/RemoveVenue";

export default function Venue({ venue, dispatch }) {
  // State
  const [showDropDownMenu, setShowDropDownMenu] = useState(false);
  const [modalStatus, setModalStatus] = useState("none");

  const handleDropdownButtonClick = () => {
    showDropDownMenu ? setShowDropDownMenu(false) : setShowDropDownMenu(true);
  };

  // When the 'modalStatus' changes,
  // the dropdown menu closes.
  useEffect(() => {
    setShowDropDownMenu(false);
  }, [modalStatus]);

  // Define the modal title depending on the modal status.
  const modalTitle =
    modalStatus === "editing" ? `Update ${venue.name}` : `Delete ${venue.name}`;

  return (
    <div className="mb-5 mx-auto rounded-lg border border-gray-200 shadow-md">
      <div className="flex justify-end px-4 pt-4">
        <div className="flex flex-col absolute">
          {/* Dropdown menu button */}
          <button
            onClick={handleDropdownButtonClick}
            id="dropdownButton"
            data-dropdown-toggle="dropdown"
            className="sm:inline-block self-end text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-4 focus:ring-gray-200 rounded-lg text-sm p-1.5"
            type="button"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path d="M10 6a2 2 0 110-4 2 2 0 010 4zM10 12a2 2 0 110-4 2 2 0 010 4zM10 18a2 2 0 110-4 2 2 0 010 4z" />
            </svg>
          </button>

          {/* Dropdown menu */}
          {showDropDownMenu && (
            <div
              id="dropdown"
              className="relative top-0 right-0 mt-1 bg-white z-10 w-44 text-base list-none rounded divide-y divide-gray-100 shadow"
            >
              <ul className="py-1" aria-labelledby="dropdownButton">
                <li>
                  <button
                    onClick={() => setModalStatus("editing")}
                    className="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 w-full text-left"
                  >
                    Edit
                  </button>
                </li>

                <li>
                  <button
                    onClick={() => setModalStatus("removing")}
                    className="block py-2 px-4 text-sm text-red-600 hover:bg-gray-100 w-full text-left"
                  >
                    Delete
                  </button>
                </li>
              </ul>
            </div>
          )}
        </div>
      </div>

      {/* The modal */}
      {modalStatus !== "none" && (
        <Modal title={modalTitle} XClick={() => setModalStatus("none")}>
          {modalStatus === "editing" && (
            <EditVenue
              venue={venue}
              dispatch={dispatch}
              setModalStatus={setModalStatus}
            />
          )}

          {modalStatus === "removing" && (
            <RemoveVenue
              venue={venue}
              dispatch={dispatch}
              setModalStatus={setModalStatus}
            />
          )}
        </Modal>
      )}

      {/* Venue */}
      <div className="flex flex-col items-center pb-10 mt-12">
        <h3 className="mb-1 text-xl font-medium text-gray-900">{venue.name}</h3>

        <span className="text-sm text-gray-500">{venue.address}</span>
      </div>
    </div>
  );
}
