import Venue from "./Venue";

export default function VenueList({ venues, dispatch }) {
  if (venues.length === 0) {
    return <p>There are no venues.</p>;
  }

  return venues.map(venue => (
    <Venue key={venue._id} venue={venue} dispatch={dispatch} />
  ));
}
