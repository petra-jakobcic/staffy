import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { Disclosure } from "@headlessui/react";
import { MenuIcon, XIcon } from "@heroicons/react/outline";

const navigation = [
  { name: "Home", uri: "/" },
  { name: "Staff", uri: "/staff" },
  { name: "Venues", uri: "/venues" }
  // { name: "Products", uri: "#" },
  // { name: "Sales", uri: "#" },
  // { name: "Stock", uri: "#" },
  // { name: "Work Schedule", uri: "#" }
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Navigation() {
  // Get the current uri.
  const { pathname } = useLocation();

  return (
    <Disclosure as="nav" className="relative bg-gray-800">
      {({ open }) => (
        <>
          <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
            <div className="relative flex items-center justify-between h-16">
              <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                {/* Mobile menu button*/}
                <Disclosure.Button className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                  <span className="sr-only">Open main menu</span>
                  {open ? (
                    <XIcon className="block h-6 w-6" aria-hidden="true" />
                  ) : (
                    <MenuIcon className="block h-6 w-6" aria-hidden="true" />
                  )}
                </Disclosure.Button>
              </div>

              <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                <div className="hidden sm:block sm:ml-6">
                  <div className="flex space-x-4">
                    {navigation.map(link => (
                      <NavLink link={link} key={link.name} />
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>

          <Disclosure.Panel className="sm:hidden absolute bg-gray-800 w-full top-0 left-0 mt-16">
            <div className="px-2 pt-2 pb-3 space-y-1">
              {navigation.map(link => (
                <Disclosure.Button
                  key={link.name}
                  as="div"
                  className={classNames(
                    link.uri === pathname
                      ? "bg-gray-900 text-white"
                      : "text-gray-300 hover:bg-gray-700 hover:text-white",
                    "block px-3 py-2 rounded-md text-base font-medium"
                  )}
                  aria-current={link.uri === pathname ? "page" : undefined}
                >
                  <NavLink link={link} />
                </Disclosure.Button>
              ))}
            </div>
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  );
}

function NavLink({ link }) {
  const { pathname } = useLocation();

  return (
    <Link
      to={link.uri}
      className={classNames(
        link.uri === pathname
          ? "bg-gray-900 text-white"
          : "text-gray-300 hover:bg-gray-700 hover:text-white",
        "px-3 py-2 rounded-md text-sm font-medium"
      )}
      aria-current={link.uri === pathname ? "page" : undefined}
    >
      {link.name}
    </Link>
  );
}
