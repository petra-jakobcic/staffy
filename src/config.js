export const staffURL = "http://localhost:5000/staff";
export const cloudinaryUploadURL =
  "https://api.cloudinary.com/v1_1/pony-bear/image/upload";
export const venueURL = "http://localhost:5000/venues";
