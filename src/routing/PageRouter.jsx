import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navigation from "../components/Navigation";
import HomePage from "../pages/HomePage";
import StaffPage from "../pages/StaffPage";
import VenuePage from "../pages/VenuePage";

export default function PageRouter() {
  return (
    <Router>
      <Navigation />

      <Routes>
        {/* Remember to use "exact" if the beginning of the path is used in other routes. */}
        <Route path="/" exact element={<HomePage />} />
        <Route path="/staff" exact element={<StaffPage />} />
        <Route path="/venues" exact element={<VenuePage />} />
      </Routes>
    </Router>
  );
}
