import PageRouter from "./routing/PageRouter";

export default function App() {
  return (
    <>
      <PageRouter />
    </>
  );
}
