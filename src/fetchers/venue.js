import { venueURL } from "../config";

/**
 * Gets all the venues.
 *
 * @returns {Promise} A promise which gives all the venues.
 */
export function getVenues() {
  return fetch(venueURL).then(response => response.json());
}

/**
 * Creates a new venue.
 *
 * @param {string} name The venue's name.
 * @param {string} address The venue's address.
 * @param {string} postcode The venue's postcode.
 * @param {Boolean} isOneOff Determines whether a venue is a one-off venue.
 * @returns {Promise} A promise which gives the new venue.
 */
export function addVenue(name, address, postcode, isOneOff = false) {
  return fetch(venueURL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      name,
      address,
      postcode,
      isOneOff
    })
  }).then(response => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error("Non 2** response!");
    }
  });
}

/**
 * Deletes a venue.
 *
 * @param {string} id The venue's ID.
 * @returns {Promise} A promise which gives a message about the deleted venue.
 */
export function removeVenue(id) {
  return fetch(`${venueURL}/${id}`, {
    method: "DELETE"
  }).then(response => response.json());
}

/**
 * Updates a venue.
 *
 * @param {string} id The venue's ID.
 * @param {Object} updatedVenue The updated venue.
 *
 * @returns {Promise} A promise which gives the updated venue.
 */
export function updateVenue(id, updatedVenue) {
  return fetch(`${venueURL}/${id}`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      name: updatedVenue.name,
      address: updatedVenue.address,
      postcode: updatedVenue.role,
      isOneOff: updatedVenue.isOneOff
    })
  }).then(response => response.json());
}
