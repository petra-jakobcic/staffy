import { cloudinaryUploadURL, staffURL } from "../config";

/**
 * Gets all the employees.
 *
 * @returns {Promise} A promise which gives all the employees.
 */
export function getEmployees() {
  return fetch(staffURL).then(response => response.json());
}

/**
 * Creates a new employee.
 *
 * @param {string} firstName The employee's first name.
 * @param {string} lastName The employee's last name.
 * @param {string} role The employee's role.
 * @param {string} publicId The employee's photo.
 * @returns {Promise} A promise which gives the new employee.
 */
export function addEmployee(firstName, lastName, role, publicId) {
  return fetch(staffURL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      firstName,
      lastName,
      role,
      publicId
    })
  }).then(response => response.json());
}

/**
 * Deletes an employee.
 *
 * @param {string} id The employee's ID.
 * @returns {Promise} A promise which gives a message about the deleted employee.
 */
export function removeEmployee(id) {
  return fetch(`${staffURL}/${id}`, {
    method: "DELETE"
  }).then(response => response.json());
}

/**
 * Updates an employee.
 *
 * @param {string} id The employee's ID.
 * @param {Object} updatedEmployee The updated employee.
 *
 * @returns {Promise} A promise which gives the updated employee.
 */
export function updateEmployee(id, updatedEmployee) {
  return fetch(`${staffURL}/${id}`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      firstName: updatedEmployee.firstName,
      lastName: updatedEmployee.lastName,
      role: updatedEmployee.role
    })
  }).then(response => response.json());
}

/**
 * Uploads an employee image.
 *
 * @param {File} image The image data.
 *
 * @returns {Promise} A promise which gives the image data.
 */
export function uploadImage(image) {
  const data = new FormData();

  // Request payload.
  data.append("file", image);
  data.append("upload_preset", "u1l3f1hl");
  data.append("cloud_name", "pony-bear");

  return fetch(cloudinaryUploadURL, {
    method: "POST",
    body: data
  }).then(response => response.json());
}
